﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Imposto.Core.Domain;
using System.Collections.Generic;
using Imposto.Core.Service;

namespace UnitTestImposto
{
    [TestClass]
    public class UnitTest1
    {



        [TestMethod]
        public void Teste_GerarXML()
        {
            string path = @"C:\notas\";
            NotaFiscal nota = new NotaFiscal();
            nota.NomeCliente = "Paulemberg";
            nota.EstadoOrigem = "SP";
            nota.EstadoDestino = "SP";
            nota.ItensDaNotaFiscal.Add(new NotaFiscalItem());
            Imposto.Core.Utils.SerializeXml.Salvar(path, nota);

        }
        [TestMethod]
        public void Teste_GerarNotaFiscal()
        {
            Pedido pedido = new Pedido();
            pedido.EstadoDestino = "SP";
            pedido.EstadoOrigem = "MG";
            pedido.NomeCliente = "Paulemberg";
            pedido.ItensDoPedido = new List<PedidoItem>
            {
                new PedidoItem
                {
                    CodigoProduto = "1234",
                    NomeProduto = "Caneta",
                    ValorItemPedido = 15.00,
                },

                 new PedidoItem
                {
                    CodigoProduto = "321",
                    NomeProduto = "Caderno",
                    ValorItemPedido = 115,
                }
            };

            NotaFiscalService service = new NotaFiscalService();
            service.GerarNotaFiscal(pedido);            
        }

       

    }
}
