ALTER PROCEDURE PR_TOTALIZANDO_ICMS_IPI

AS 

BEGIN

SELECT  Cfop,
		SUM(BaseIcms) AS BaseIcms,
		SUM(ValorIcms) AS ValorIcms,
		SUM(BaseIpi) AS BaseIpi, 
		SUM(ValorIpi) AS ValorIpi 
  FROM  NotaFiscalItem
  GROUP BY Cfop

  END


