﻿using Imposto.Core.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Imposto.Core.Utils
{
    public class SerializeXml
    {

        public static T Ler<T>(string caminhoArquivo)
        {
            T obj = default(T);
            XmlSerializer x = new XmlSerializer(typeof(T));
            using (FileStream st = File.Open(caminhoArquivo, FileMode.Open))
            {
                obj = (T)x.Deserialize(st);
            }
            return obj;
        }

        public static void Salvar<T>(string caminhoArquivo, T obj)
        {
            XmlSerializer x = new XmlSerializer(typeof(T));
            using (FileStream st = File.Open(caminhoArquivo, FileMode.Create))
            {
                x.Serialize(st, obj);
            }
        }




    }
}
