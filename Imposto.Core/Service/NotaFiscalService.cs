﻿using Imposto.Core.Data;
using Imposto.Core.Domain;
using Imposto.Core.Utils;

namespace Imposto.Core.Service
{
    public class NotaFiscalService
    {
        private string path = System.Configuration.ConfigurationSettings.AppSettings["DIRETORIO_XML_IFRA"];

        public string Path
        {
            get
            {
                return path;
            }

            set
            {
                path = value;
            }
        }

        public void GerarNotaFiscal(Pedido pedido)
        {
            NotaFiscal notaFiscal = new NotaFiscal();
            notaFiscal.EmitirNotaFiscal(pedido);
            try
            {
                SerializeXml.Salvar(Path + "NF-" + notaFiscal.Serie + ".xml", notaFiscal);
                //Insert(notaFiscal);

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
            
            
        }

        public void Insert(NotaFiscal nota)
        {
            ImpostoContext context = new ImpostoContext();
            context.NotaFiscal.Add(nota);
        }

        public void Delete(int id)
        {
            Delete(id);
        }

        public void Update(NotaFiscal nota)
        {
            Update(nota);
        }

        public void GerarXmlNotaFiscal(NotaFiscal nota)
        {

        }
    }
}
