﻿using Imposto.Core.Domain;
using System;


namespace Imposto.Core.Data
{
    public interface INotaFiscalRepository
    {
        void Insert(NotaFiscal notaFiscal);
        void Delete(int id);
        void Update(NotaFiscal notaFiscal);
    }

    
}
