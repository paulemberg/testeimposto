﻿using Imposto.Core.Domain;
using System.Data.Entity;

namespace Imposto.Core.Data
{
    public class ImpostoContext : DbContext
           {
        public ImpostoContext() : base ("name=SqlConnection")
        {

        }
        
        public DbSet<NotaFiscal> NotaFiscal { get; set; }
    }
}
