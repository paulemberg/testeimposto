﻿using Imposto.Core.Domain;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System;

namespace Imposto.Core.Data
{
    
    public class NotaFiscalRepository : INotaFiscalRepository
    {
        private ImpostoContext context;

        public void Insert(NotaFiscal nota)
        {
         
        }

        public void Delete (int id) {}

        public void Update(NotaFiscal nota)
        {
            context.Entry(nota).State = EntityState.Modified;
        }



        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
