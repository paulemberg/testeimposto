﻿using Imposto.Core.Service;
using System;
using System.Data;
using System.Windows.Forms;
using Imposto.Core.Domain;
using Imposto.Core.Data;

namespace TesteImposto
{
    public partial class FormImposto : Form
    {
        private Pedido pedido = new Pedido();

        public FormImposto()
        {
            InitializeComponent();
            dataGridViewPedidos.AutoGenerateColumns = true;                       
            dataGridViewPedidos.DataSource = GetTablePedidos();
            ResizeColumns();
        }

        private void ResizeColumns()
        {
            double mediaWidth = dataGridViewPedidos.Width / dataGridViewPedidos.Columns.GetColumnCount(DataGridViewElementStates.Visible);

            for (int i = dataGridViewPedidos.Columns.Count - 1; i >= 0; i--)
            {
                var coluna = dataGridViewPedidos.Columns[i];
                coluna.Width = Convert.ToInt32(mediaWidth);
            }   
        }

        private object GetTablePedidos()
        {
            DataTable table = new DataTable("pedidos");
            table.Columns.Add(new DataColumn("Nome do produto", typeof(string)));
            table.Columns.Add(new DataColumn("Codigo do produto", typeof(string)));
            table.Columns.Add(new DataColumn("Valor", typeof(decimal)));
            table.Columns.Add(new DataColumn("Brinde", typeof(bool)));
                     
            return table;
        }

        private void buttonGerarNotaFiscal_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxNomeCliente.Text))
            {
                MessageBox.Show("Nome Cliente não pode ser vazio", "Atenção", MessageBoxButtons.OK);
                textBoxNomeCliente.Focus();
                return;
            }

            if (string.IsNullOrEmpty(txtEstadoOrigem.Text))
            {
                MessageBox.Show("Estado Origem não pode ser vazio", "Atenção",  MessageBoxButtons.OK);
                txtEstadoOrigem.Focus();
                return;
            }

            if (string.IsNullOrEmpty(txtEstadoDestino.Text))
            {
                MessageBox.Show("Estado Destino não pode ser vazio", "Atenção", MessageBoxButtons.OK);
                txtEstadoDestino.Focus();
                return;
            }

         
            NotaFiscalService service = new NotaFiscalService();
            pedido.EstadoOrigem = txtEstadoOrigem.Text;
            pedido.EstadoDestino = txtEstadoDestino.Text;
            pedido.NomeCliente = textBoxNomeCliente.Text;
            
            DataTable table = (DataTable)dataGridViewPedidos.DataSource;

            foreach (DataRow row in table.Rows)
            {
                bool brinde = false;
                if (row["Brinde"].Equals(true))
                {
                    brinde = true;
                };

                pedido.ItensDoPedido.Add(
                    new PedidoItem()
                    {
                        Brinde = brinde,
                        CodigoProduto =  row["Codigo do produto"].ToString(),
                        NomeProduto = row["Nome do produto"].ToString(),
                        ValorItemPedido = Convert.ToDouble(row["Valor"].ToString())            
                    });
            }

                       
            service.GerarNotaFiscal(pedido);

            dataGridViewPedidos.DataSource = null;
            dataGridViewPedidos.Rows.Clear();
            txtEstadoOrigem.Clear();
            txtEstadoDestino.Clear();
            textBoxNomeCliente.Clear();

            MessageBox.Show("Operação efetuada com sucesso");
        }
    }
}
